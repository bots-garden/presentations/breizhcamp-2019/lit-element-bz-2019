import { LitElement, html } from 'lit-element'
import {styles} from './styles.js'
import {render} from 'lit-html'

/* --- componennts --- */
import {MyTitle} from './my-title.js'
// my-sub-title
// my-amazing-button
// buddies-list
// my-field
// buddy-form

// components for router

export class MainApplication extends LitElement {

  // properties

  constructor() {
    super()
    render(html`
      ${styles}
      <style>
          html,body {
            font-family: 'robotoregular';
          }
      </style>
    `, document.head)  

    // event listener


    // initialize subComponent & router handling
    
  }

  // on message

  render() {
    return html`
    ${styles}
      <section class="section">
        <div class="container">
          <my-title></my-title>
          <!-- my-sub-title -->

        </div>
      </section>

      <!-- links for router -->

      <section class="section">
        <div class="container has-text-centered">
          <!-- my-field 👋⚠️shadowroot -->

          <!-- buddy-form -->
        </div>
      </section>

      <section class="section">
        <div class="container has-text-centered">
          <!-- buddies-list -->

        </div>
      </section>


      <section class="section">
        <div class="container has-text-centered">
          <!-- my-amazing-button x 2 ⚠️ -->

        </div>
      </section>


    `
  }
}

customElements.define('main-application', MainApplication)
