# DEMO

- open 2 terminals
- first: `cd src`
- second: `polymer serve`

## Run

```shell
polymer serve
```


## Build

```shell
polymer build
polymer serve build/es6-bundled
```