import { LitElement, html } from 'lit-element'

import {MyTitle} from './my-title.js'
import {MyCounter} from './my-counter.js'
import {IncrementButton} from './increment-button.js'
import {DecrementButton} from './decrement-button.js'

import {styles} from '../styles/styles.js';
import {render} from 'lit-html'


export class MainApplication extends LitElement {

  constructor() {
    super()
    render(html`
      ${styles}
      <style>
          html,body {
            font-family: 'robotoregular';
          }
      </style>
    `, document.head)  

  }

  render() {
    return html`
      ${styles}

      <section class="section">
        <div class="container has-text-centered">
          <my-title></my-title>
        </div>
      </section>

      <section class="section">
        <div class="container has-text-centered">
          <my-counter></my-counter>
        </div>
      </section>

      <section class="section">
        <div class="container has-text-centered">
          <decrement-button></decrement-button>
          <increment-button></increment-button>
        </div>
      </section>
    `
  }
}

customElements.define('main-application', MainApplication)
