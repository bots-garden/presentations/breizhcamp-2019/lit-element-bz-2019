// Import redux store logic
//import { createStore } from 'redux'
/*
  <script>
    // Redux assumes `process.env.NODE_ENV` exists in the ES module build.
    // https://github.com/reactjs/redux/issues/2907
    // window.process = { env: { NODE_ENV: 'production' } }
  </script>
*/
import { createStore } from 'redux/es/redux.mjs'

// reducer
function simpleReducer(state = {counter:42, message: "👋"}, action) {
  switch (action.type) {
    case 'INCREMENT':
      return Object.assign(state, {
        counter: state.counter + 1,
        message: "Incrementing..."
      })
    case 'DECREMENT':
      return Object.assign(state, {
        counter: state.counter - 1,
        message: "Decrementing..."
      })
    default:
      return state
  }
}
// Create and export store so it can be imported and shared by app elements
export const store = createStore(simpleReducer)