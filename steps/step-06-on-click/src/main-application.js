import { LitElement, html } from 'lit-element'

import {MyTitle} from './my-title.js'
import {MySubTitle} from './my-sub-title.js'
import {MyAmazingButton} from './my-amazing-button.js'

import {styles} from './styles.js';

import {render} from 'lit-html'

export class MainApplication extends LitElement {
  constructor() {
    super()
    render(html`
      ${styles}
      <style>
          html,body {
            font-family: 'robotoregular';
          }
      </style>
    `, document.head)  
  }
  render() {
    return html`
      ${styles}

      <section class="section">
        <div class="container has-text-centered">
          
          <my-title></my-title>
          <my-sub-title my-text="I ❤️ LitElement"></my-sub-title>

        </div>
      </section>

      <section class="section">
        <div class="container has-text-centered">

          <my-amazing-button></my-amazing-button>
          <my-amazing-button></my-amazing-button>

        </div>
      </section>

    `
  }
}

customElements.define('main-application', MainApplication)
