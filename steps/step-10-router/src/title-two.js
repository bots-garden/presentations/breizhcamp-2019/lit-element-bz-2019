import { LitElement, html } from 'lit-element';
import {styles} from './styles.js';

export class TitleTwo extends LitElement {
  render(){
    return html`
      ${styles}
      <h2 class="title is-2">Two</h2>
    `
  }
}
customElements.define('title-two', TitleTwo)
