import { LitElement, html } from 'lit-element';
import {styles} from './styles.js';

export class TitleThree extends LitElement {
  render(){
    return html`
      ${styles}
      <h3 class="title is-3">Three</h3>
    `
  }
}
customElements.define('title-three', TitleThree)
