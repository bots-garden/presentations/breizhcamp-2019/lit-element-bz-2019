import { LitElement, html } from 'lit-element'

import {MyTitle} from './my-title.js'
import {MySubTitle} from './my-sub-title.js'
import {MyAmazingButton} from './my-amazing-button.js'
import {BuddiesList} from './buddies-list.js'
import {MyField} from './my-field.js'
import {BuddyForm} from './buddy-form'

import {TitleOne} from './title-one'
import {TitleTwo} from './title-two'
import {TitleThree} from './title-three'
import { installRouter } from 'pwa-helpers/router.js'


import {styles} from './styles.js';
import {render} from 'lit-html'

// https://k33g.gitlab.io/articles/2019-01-13-LITELEMENT-04.html

export class MainApplication extends LitElement {


  static get properties() {
    return {
      subComponent: { type: Object }
    }
  }


  constructor() {
    super()
    render(html`
      ${styles}
      <style>
          html,body {
            font-family: 'robotoregular';
          }
      </style>
    `, document.head)  

    // the main application is listening
    this.addEventListener('add-buddy', this.onMessage)

    // router demo
    this.subComponent = html`<title-one></title-one>`

    installRouter((location) => {
      console.log(location)
      switch (location.hash) {
        case "#display/one":
          this.subComponent = html`<title-one></title-one>`
          break
        case "#display/two":
          this.subComponent = html`<title-two></title-two>`
          break
        case "#display/three":
          this.subComponent = html`<title-three></title-three>`
          break
        default:
          break
      }
    })
  }

  onMessage(message) {
    if(message.type==='add-buddy') {
      console.log(message.detail)
      this.shadowRoot.querySelector('buddies-list').addBuddy(message.detail)
    }    
  }

  render() {
    return html`
      ${styles}

      <section class="section">
        <div class="container has-text-centered">
          
          <my-title></my-title>
          <my-sub-title my-text="I ❤️ LitElement"></my-sub-title>

        </div>
      </section>

      <section class="section">
        <div class="container has-text-centered">
          <a class="tag is-large is-dark" href="/#display/one">one</a> 
          <a class="tag is-large is-link" href="/#display/two">two</a>
          <a class="tag is-large is-primary" href="/#display/three">three</a>

          ${this.subComponent}
        </div>
      </section>      

      <section class="section">
        <div class="container has-text-centered">

          <buddy-form></buddy-form>

          <my-field>🦊</my-field>

        </div>
      </section>
      
      <section class="section">
        <div class="container has-text-centered">
          <buddies-list></buddies-list>
        </div>
      </section>

      <section class="section">
        <div class="container has-text-centered">
          <my-amazing-button></my-amazing-button>
        </div>
      </section>

    `
  }
}

customElements.define('main-application', MainApplication)
