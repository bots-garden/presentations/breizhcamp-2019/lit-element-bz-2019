import { LitElement, html } from 'lit-element';
import {styles} from './styles.js';

export class BuddyForm extends LitElement {

  render(){

    return html`

      ${styles}

      <div class="field">
        <div class="control">
          <input 
            id="avatar"
            class="input is-medium" 
            type="text" 
            placeholder="🎃"
          >
        </div>
      </div>
      <div class="field">
        <div class="control">
          <input 
            id="name"
            class="input is-medium" 
            type="text" 
            placeholder="John Doe"
          >
        </div>
      </div>

      <div class="field">
        <div class="control">
          <a 
            @click="${this.onClick}"
            class="button is-medium is-dark is-rounded">
            Add buddy
          </a>
        </div>
      </div>  
    `
  }

  get avatar() {
    return this.shadowRoot.getElementById('avatar').value
  }

  get name() {
    return this.shadowRoot.getElementById('name').value
  }

  set avatar(value) {
    this.shadowRoot.getElementById('avatar').value = value
  }

  set name(value) {
    this.shadowRoot.getElementById('name').value = value
  }


  onClick() {
    this.dispatchEvent(
      new CustomEvent('add-buddy', {  
        bubbles: true, composed: true, detail: {
          name: this.name, avatar: this.avatar
        } 
      })
    )
    this.avatar = ""
    this.name = ""
  }

}
customElements.define('buddy-form', BuddyForm)