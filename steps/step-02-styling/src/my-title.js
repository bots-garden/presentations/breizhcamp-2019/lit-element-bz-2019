import { LitElement, html } from 'lit-element'
import {style} from './main-styles.js'

export class MyTitle extends LitElement {

  static get styles() { return [style] }

  render(){
    return html`
      <h1 class="title">
        Skeleton 🤖 [LitElement]
      </h1> 
    `
  }
}
customElements.define('my-title', MyTitle)