import { LitElement, html } from 'lit-element';
import {styles} from './styles.js';

export class BuddiesList extends LitElement {

  static get properties() {
    return {
      buddies: { type: Array }
    }
  }

  constructor() {
    super()
    this.buddies = [
      {avatar: "🦊", name: "Foxy"},
      {avatar: "🦁", name: "Leo"},
      {avatar: "🐯", name: "Tigrou"}
    ]
  }

  render(){
    return html`
      ${styles}
      <table class="table table is-fullwidth is-size-3">
        <thead>
          <tr><th>avatar</th><th>name</th></tr>
        </thead>
        <tbody>
          ${this.buddies.map(buddy => 
            html`
              <tr>
                <td>${buddy.avatar}</td>
                <td>${buddy.name}</td>
              </tr>
            `
          )}
        </tbody>
      </table>
    `
  }
}
customElements.define('buddies-list', BuddiesList)

