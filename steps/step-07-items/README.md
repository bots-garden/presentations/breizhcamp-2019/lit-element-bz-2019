## Run

```shell
polymer serve
```


## Build

```shell
polymer build
polymer serve build/es6-bundled
```